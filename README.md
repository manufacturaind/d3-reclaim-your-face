# d3-reclaim-your-face


Editable SVG files for EDRi's campaign [Reclaim your face](https://reclaimyourface.eu/); plus D3 Portuguese translated images.

Original pack with English text at: https://cloud.edri.org/index.php/s/9zDJ6kx6tJpANyH?path=%2FVisuals